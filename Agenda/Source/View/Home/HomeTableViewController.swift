//
//  HomeTableViewController.swift
//  Agenda
//
//  Created by diego.crozare on 05/04/2019.
//  Copyright © 2017 Alura. All rights reserved.
//

import UIKit
import CoreData

class HomeTableViewController: UITableViewController, UISearchBarDelegate, NSFetchedResultsControllerDelegate, Storyboarded {
    
    //MARK: - Constants
    
    let searchController = UISearchController(searchResultsController: nil)

    //MARK: - Variáveis
    
    var coordinator: MainCoordinator?
    var gerenciador: NSFetchedResultsController<Aluno>?
    var contexto: NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        return appDelegate.persistentContainer.viewContext
    }
    var aluno: AlunoViewController?
    
    //MARK: - IBOutlets
    
    @IBOutlet var tableview: UITableView?
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let navigation = self.navigationController {
            self.coordinator = MainCoordinator(navigation)
        }
        self.configuraSearch()
        setupDados()
    }
    
    // MARK: - IBActions
    
    @IBAction func addAluno(_ sender: Any) {
        coordinator?.cadastroAluno()
    }
    
    @IBAction func averageStudent(_ sender: UIBarButtonItem) {
        guard let alunos = gerenciador?.fetchedObjects else { return }
        ServiceSchedule.shared.averageStudent(student: alunos) { (student, error) in
            if error == nil {
                print(student)
            }
        }
    }
    
    // MARK: - Méthods
    
    func setupDados() {
        let fetchData: NSFetchRequest<Aluno> = Aluno.fetchRequest()
        let orderData =  NSSortDescriptor(key: "nome", ascending: true)
        
        fetchData.sortDescriptors = [orderData]
        gerenciador = NSFetchedResultsController(fetchRequest: fetchData, managedObjectContext: contexto, sectionNameKeyPath: nil, cacheName: nil)
        gerenciador?.delegate = self
        
        do {
            try  gerenciador?.performFetch()
        } catch {
            print(error.localizedDescription)
        }
        
        self.tableview?.reloadData()
    }
    
    func configuraSearch() {
        self.searchController.searchBar.delegate = self
        self.searchController.dimsBackgroundDuringPresentation = false
        self.navigationItem.searchController = searchController
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .delete:
            guard let indexPath = indexPath else { return }
            tableView.deleteRows(at: [indexPath], with: .fade)
            break
        default:
            tableview?.reloadData()
        }
    }
    
    @objc func actionGesture(_ gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            guard let recuperandoAluno = gerenciador?.fetchedObjects?[(gesture.view?.tag)!] else { return }
            let menu = MenuActionSheet().menuAction { (opcao) in
                switch opcao {
                case .sms:
                    let message = Mensagem()
                    if let messageResult = message.messageSMS(recuperandoAluno) {
                        messageResult.messageComposeDelegate = message
                        self.present(messageResult, animated: true, completion: nil)
                    }
                case .phone:
                    guard let phone = recuperandoAluno.telefone else { return }
                    if let url = URL(string: "tel://\(phone)"), UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url, options:[:], completionHandler: nil)
                    }
                case .wase:
                    
                    if let wase = URL(string: "wase://"), UIApplication.shared.canOpenURL(wase) {
                        guard let student = recuperandoAluno.endereco else { return }
                        MapsLocation().LocationStudent(string: student, completion: { (location) in
                            let latitude = String(describing: location.location?.coordinate.latitude)
                            let longitude = String(describing: location.location?.coordinate.longitude)
                            
                            guard let urlWase = URL(string: "wase://?ll=\(latitude), \(longitude)&navigate=yes") else { return }
                            UIApplication.shared.open(urlWase, options: [:], completionHandler: nil)
                        })
                    } else {
                        guard let installWase = URL(string: "http://itunes.apple.com/us/app/id323229106") else { return }
                        UIApplication.shared.open( installWase, options: [:], completionHandler: nil)
                    }
                case .map:
                    self.coordinator?.mapsStudent(recuperandoAluno)
                }
            }
            self.present(menu, animated: true, completion: nil)
        }
        
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let data = gerenciador?.fetchedObjects?.count else {
            return 0
        }
        return data
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "celula-aluno", for: indexPath) as! HomeTableViewCell
        
        guard let aluno = gerenciador?.fetchedObjects?[indexPath.row] else { return cell }
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(actionGesture(_:)))
        if let imagem = aluno.foto as? UIImage {
            cell.imageAluno.image = imagem
        }
        cell.labelNomeDoAluno.text = aluno.nome
        cell.addGestureRecognizer(gesture)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            guard let alunoSelecionado = gerenciador?.fetchedObjects?[indexPath.row] else { return }
            
            contexto.delete(alunoSelecionado)
            
            do {
                try contexto.save()
            } catch {
                print(error.localizedDescription)
            }
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let alunoSelecionado = gerenciador?.fetchedObjects?[indexPath.row] else { return }
        coordinator?.cadastroAluno(alunoSelecionado)
    }
    
}
