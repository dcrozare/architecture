//
//  MapsViewController.swift
//  Agenda
//
//  Created by diego.crozare on 07/04/19.
//  Copyright © 2019 Alura. All rights reserved.
//

import UIKit
import MapKit

class MapsViewController: UIViewController, Storyboarded {

    @IBOutlet weak var map: MKMapView!
    
    var aluno: Aluno?
    var coordinator: Coordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Localizar aluno"
        locationMaps()
        locationUser()
    }
    
    func locationMaps() {
        MapsLocation().LocationStudent(string: "Caelum - São Paulo") { (location) in
            let pino = self.configPin(title: "Caelum", location: location)
            let region = MKCoordinateRegion(center: pino.coordinate, latitudinalMeters: 5000, longitudinalMeters: 5000)
            
            self.map.setRegion(region, animated: true)
            self.map.addAnnotation(pino)
        }
    }
    
    func locationUser() {
        if let aluno = aluno {
            MapsLocation().LocationStudent(string: aluno.endereco ?? "") { (location) in
                let pino = self.configPin(title: aluno.nome ?? "", location: location)
                 self.map.addAnnotation(pino)
            }
        }
        
    }
    
    func configPin(title: String, location: CLPlacemark) -> MKPointAnnotation {
        let pino = MKPointAnnotation()
        pino.title = title
        pino.coordinate = location.location!.coordinate
        
        return pino
    }
    
}
