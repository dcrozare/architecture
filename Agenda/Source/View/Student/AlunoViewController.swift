//
//  AlunoViewController.swift
//  Agenda
//
//  Created by diego.crozare on 05/04/2019.
//  Copyright © 2017 Alura. All rights reserved.
//

import UIKit
import CoreData

class AlunoViewController: UIViewController, Storyboarded {
    
    //MARK: - Constants
    
    let imagePicker = ImagePicker()
    
    //MARK: - Properties
    
    weak var coordinator: MainCoordinator?
    var contexto: NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    var aluno: Aluno?
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var viewImagemAluno: UIView!
    @IBOutlet weak var imageAluno: UIImageView?
    @IBOutlet weak var buttonFoto: UIButton?
    @IBOutlet weak var scrollViewPrincipal: UIScrollView?
    @IBOutlet weak var textFieldNome: UITextField?
    @IBOutlet weak var textFieldEndereco: UITextField?
    @IBOutlet weak var textFieldTelefone: UITextField?
    @IBOutlet weak var textFieldSite: UITextField?
    @IBOutlet weak var textFieldNota: UITextField?
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        self.arredondaView()
        NotificationCenter.default.addObserver(self, selector: #selector(aumentarScrollView(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      setup()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: - Methods
    
    func arredondaView() {
        self.viewImagemAluno.layer.cornerRadius = self.viewImagemAluno.frame.width / 2
        self.viewImagemAluno.layer.borderWidth = 1
        self.viewImagemAluno.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    @objc func aumentarScrollView(_ notification:Notification) {
        self.scrollViewPrincipal?.contentSize = CGSize(width: self.scrollViewPrincipal?.frame.width ?? 0, height: self.scrollViewPrincipal?.frame.height ?? 0 + (self.scrollViewPrincipal?.frame.height)! / 2)
    }
    
    func setup() {
        imagePicker.delegate = self
        guard let alunoSelecionado = aluno else { return }
        textFieldNome?.text = alunoSelecionado.nome
        textFieldEndereco?.text = alunoSelecionado.endereco
        textFieldTelefone?.text = alunoSelecionado.telefone
        textFieldSite?.text = alunoSelecionado.site
        textFieldNota?.text = "\(alunoSelecionado.nota)"
        imageAluno?.image = alunoSelecionado.foto as? UIImage
    }
    
    func menuOpcao(opcao: OpcaoMultimidia) {
        let multimidia = UIImagePickerController()
        multimidia.delegate = imagePicker
        
        if opcao == .camera && UIImagePickerController.isSourceTypeAvailable(.camera) {
            multimidia.sourceType = .camera
        } else {
            multimidia.sourceType = .photoLibrary
        }
        self.present(multimidia, animated: true, completion: nil)
    }
    
    // MARK: - IBActions
    
    @IBAction func buttonFoto(_ sender: UIButton) {
        let menu = imagePicker.selectMultimidia { (opcao) in
            self.menuOpcao(opcao: opcao)
        }
        
        present(menu, animated: true, completion: nil)
    }
    
    @IBAction func stepperNota(_ sender: UIStepper) {
        self.textFieldNota?.text = "\(sender.value)"
    }
    
    @IBAction func buttonSalvar(_ sender: UIButton) {
        
        if aluno == nil {
            aluno = Aluno(context: contexto)
        }
        
        aluno?.nome = textFieldNome?.text
        aluno?.endereco = textFieldEndereco?.text
        aluno?.foto = imageAluno?.image
        aluno?.nota = (textFieldNota?.text! as NSString?)?.doubleValue ?? 0
        aluno?.telefone = textFieldTelefone?.text
        aluno?.site = textFieldSite?.text
        
        do {
            try contexto.save()
            if let navigation = navigationController {
                navigation.popViewController(animated: true)
            } else {
                dismiss(animated: true, completion: nil)
            }
            
        } catch {
            print(error.localizedDescription)
        }
    }
}

//MARK: - Extensions

//MARK: - ImagePickerDelegate
extension AlunoViewController: ImagePickerDelegate {
    func imagePickerFotoSelecionada(_ foto: UIImage) {
        
        self.imageAluno?.image = foto
    }
}
