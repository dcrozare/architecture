//
//  Mensagem.swift
//  Agenda
//
//  Created by diego.crozare on 02/04/19.
//  Copyright © 2019 Alura. All rights reserved.
//

import UIKit
import MessageUI

class Mensagem: NSObject, MFMessageComposeViewControllerDelegate {
    
    func messageSMS(_ aluno: Aluno) -> MFMessageComposeViewController? {
        
        if MFMessageComposeViewController.canSendText() {
           let message = MFMessageComposeViewController()
            message.recipients = [aluno.telefone ?? ""]
            message.messageComposeDelegate = self
            
            return message
        }
        
        return nil
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }

}
