//
//  ImagePicker.swift
//  Agenda
//
//  Created by diego.crozare on 31/03/19.
//  Copyright © 2019 Alura. All rights reserved.
//

import Foundation
import UIKit

enum OpcaoMultimidia {
    case camera
    case biblioteca
}

protocol ImagePickerDelegate {
    func imagePickerFotoSelecionada(_ foto: UIImage)
}

class ImagePicker: NSObject {

    //MARK: - Constants
    
    let attention = "Atenção"
    let option = "Escolha uma opção"
    let library = "Biblioteca"
    let takePicture = "tirar foto"
    
    //MARK: - Properties

    var delegate: ImagePickerDelegate?
    
    //MARK: - Methods
    
    func selectMultimidia(completion:@escaping (_ option: OpcaoMultimidia) -> Void) -> UIAlertController {
        let menu = UIAlertController(title: attention, message: option, preferredStyle: .actionSheet)
        
        let camera = UIAlertAction(title: takePicture, style: .default) { (camera) in
            completion(.camera)
        }
        
        let biblioteca = UIAlertAction(title: library, style: .default) { (biblioteca) in
            completion(.biblioteca)
        }
        
        menu.addAction(camera)
        menu.addAction(biblioteca)
        
        return menu
    }
}

//MARK: - EXTENSION

//MARK: - UIImagePickerControllerDelegate

extension ImagePicker: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        let foto = info[.originalImage] as! UIImage
        delegate?.imagePickerFotoSelecionada(foto)
        picker.dismiss(animated: true, completion: nil)
    }
}

//MARK: - UINavigationControllerDelegate

extension ImagePicker: UINavigationControllerDelegate {
}
