//
//  MenuActionSheet.swift
//  Agenda
//
//  Created by diego.crozare on 02/04/19.
//  Copyright © 2019 Alura. All rights reserved.
//

import Foundation
import UIKit

enum MenuAluno {
    case sms
    case phone
    case wase
    case map
}

class MenuActionSheet {
    
    //MARK: - Constants
    
    let attention = "Atencao"
    let description = "escolha uma opcao"
    let sms = "Sms"
    let wase = "Wase"
    let map = "Mapa"
    let phone = "Phone"
    let cancel = "Cancel"
    
    //MARK: - Methods
    
    func menuAction(completion: @escaping(_ opcao: MenuAluno) -> Void) -> UIAlertController {
        let alertController = UIAlertController(title: attention, message: description, preferredStyle: .actionSheet)
        
        let actionSms = UIAlertAction(title: sms, style: .default) { (_) in
            completion(.sms)
        }
        
        let actionPhone = UIAlertAction(title: phone, style: .default) { (_) in
            completion(.phone)
        }
        
        let actionWase = UIAlertAction(title: wase, style: .default) { (_) in
            completion(.wase)
        }
        
        let actionMap = UIAlertAction(title: map, style: .default) { (_) in
            completion(.map)
        }
        
        let actionCancel = UIAlertAction(title: cancel, style: .cancel, handler: nil)
        
        alertController.addAction(actionSms)
        alertController.addAction(actionPhone)
        alertController.addAction(actionWase)
        alertController.addAction(actionMap)
        alertController.addAction(actionCancel)
        
        return alertController
    }
}
