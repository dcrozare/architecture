//
//  Storyboarded.swift
//  Agenda
//
//  Created by diego.crozare on 04/04/19.
//  Copyright © 2019 Alura. All rights reserved.
//

import UIKit

/// Protocol usado para criar instancias de classes
protocol Storyboarded {
    static func instantiate() -> Self
}

//MARK: - EXTENSION

//MARK: - Storyboarded
extension Storyboarded where Self: UIViewController {
    static func instantiate() -> Self {
        
        let fullName = NSStringFromClass(self)
        let className = fullName.components(separatedBy: ".")[1]
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        return storyboard.instantiateViewController(withIdentifier: className) as! Self
    }
}
