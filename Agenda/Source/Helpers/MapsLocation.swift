//
//  MapsLocation.swift
//  Agenda
//
//  Created by diego.crozare on 07/04/19.
//  Copyright © 2019 Alura. All rights reserved.
//

import Foundation
import CoreLocation

class MapsLocation {
    
    
    func LocationStudent(string: String, completion: @escaping(_ location: CLPlacemark) -> Void) {
        let location = CLGeocoder()
        
        location.geocodeAddressString(string) { (location, error) in
            if let location = location?.first {
                completion(location)
            }
        }
    }
    
}
