//
//  MainCoordinator.swift
//  Agenda
//
//  Created by diego.crozare on 04/04/19.
//  Copyright © 2019 Alura. All rights reserved.
//

import UIKit

protocol Coordinator {
    var childCoordinator: [Coordinator] {get set}
    var navigationController: UINavigationController { get set }
    
    func start()
}

/// Classe base do design pattern coordinator 
class MainCoordinator: Coordinator {

    func start() {
    }
    
    var navigationController: UINavigationController
    var childCoordinator = [Coordinator]()
    
    init(_ navigation: UINavigationController) {
        self.navigationController = navigation
    }
    
    func cadastroAluno(_ aluno: Aluno? = nil) {
        let vc = AlunoViewController.instantiate()
        vc.coordinator = self
        vc.aluno = aluno
        navigationController.pushViewController(vc, animated: true)
    }
    
    func mapsStudent(_ aluno: Aluno? = nil) {
        let vc = MapsViewController.instantiate()
        vc.coordinator = self
        vc.aluno = aluno
        navigationController.pushViewController(vc, animated: true)
    }
}
