//
//  ApiCaelum.swift
//  Agenda
//
//  Created by diego.crozare on 07/04/19.
//  Copyright © 2019 Alura. All rights reserved.
//

import UIKit


class ApiCaelum:NSObject {
    
    //MARK: - Constant
    
    let url = "https://www.caelum.com.br/mobile"
    
    //MARK: - Methods
    
    /// Api que retorna media de alunos
    ///
    /// - Parameters:
    ///   - listStudent: lista dos alunos para realizar a soma da media
    ///   - sucesss: retorno da media dos alunos
    ///   - fail: retorno de falha do servico
    func getAverageStudent(_ listStudent: [Aluno], sucesss: @escaping(_ media: String) -> Void, fail: @escaping(_ error: Error) -> Void ) {
        
        guard let url = URL(string: url) else { return }
        
        var parameterDictionary: [[String: Any]] = [[:]]
        var json: [String: Any] = [:]
        
        parameterDictionary = studentsData(listStudent)
        
        json["list"] = [["aluno": parameterDictionary]]
       
        do {
            var request = URLRequest(url: url)
            let httpBody = try JSONSerialization.data(withJSONObject: json, options: [])
            request.httpBody = httpBody
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let data = data, error == nil {
                    
                    do {
                        let data = try JSONDecoder().decode(AverageStudent.self, from: data)
                        sucesss(data.media ?? "")
                    } catch {
                        fail(error)
                    }
                }
            }
            task.resume()
        } catch {
            print(error.localizedDescription)
        }
        
    }
    
    func studentsData(_ student: [Aluno]) -> [[String: String]] {
        var students: [[String: String]] = [[:]]
        
        student.forEach { (data) in
            let result = ["nome": data.nome ?? "",
                          "endereco": data.endereco ?? "",
                          "telefone": data.telefone ?? "",
                          "site": data.site ?? "",
                          "nota": String(describing: data.nota) ]
            
            students.append(result)
        }
        
        return students
    }
}
