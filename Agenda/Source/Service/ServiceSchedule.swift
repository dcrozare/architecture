//
//  ServiceSchedule.swift
//  Agenda
//
//  Created by diego.crozare on 04/04/19.
//  Copyright © 2019 Alura. All rights reserved.
//

import Foundation

/// Classe ServiceSchedule Singleton,...Implementação Design Pattern Singleton e Facade
class ServiceSchedule {
    
    //MARK: - Constants
    
    static let shared = ServiceSchedule()
    private let serviceApi = ApiCaelum()
    
    //MARK: - Initializer
    
    private init() {
    }
    
    //MARK: - Methods
    
    func averageStudent(student: [Aluno], completion: @escaping(String, Error?) -> Void) {
        serviceApi.getAverageStudent(student, sucesss: { (student) in
            completion(student, nil)
        }) { (error) in
            completion("", error)
        }
    }
}
