//
//  AverageStudent.swift
//  Agenda
//
//  Created by diego.crozare on 07/04/19.
//  Copyright © 2019 Alura. All rights reserved.
//

import Foundation

struct AverageStudent: Codable {
    
    let media: String?
    let quantidade: Int?
}
