//
//  Student.swift
//  Agenda
//
//  Created by diego.crozare on 08/04/19.
//  Copyright © 2019 Alura. All rights reserved.
//

import Foundation

struct Student: Codable {
    
    let endereco: String
    let foto: String
    let nome: String
    let nota:String
    let site: String
    let telefone: String
}
